FROM haproxy:1.8

WORKDIR /etc/haproxy

COPY haproxy.cfg ./haproxy.cfg
COPY ./certs.d/<key> ./certs.d/

EXPOSE 80/tcp
EXPOSE 443/tcp
EXPOSE 8102/tcp

CMD ["haproxy", "-f", "/etc/haproxy/haproxy.cfg"]