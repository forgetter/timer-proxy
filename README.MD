## Instalacja
1. Zbudowanie aplikacji React: https://gitlab.com/forgetter/react-timer (bez definicji zewnętrznych portów oraz aktualizacją portu w api/index.js na 8101).
2. Wygenerowanie klucza, umieszczenie go w katalogu certs.d oraz aktualizacja **haproxy.cfg** oraz **Dockerfile** w celu podmiany *<key>* na nazwę klucza.
3. Uruchomienie proxy SSL 
```docker build -t timer-proxy:1.0.2 .```
```docker run --rm -p 80:80 -p 443:443 -p 8102:8102 --network timer-app -d --name timer-proxy timer-proxy:1.0.2```

Jeżeli kliucz jest poprawny, aplikacja powinna działać z szyfrowaniem SSL.